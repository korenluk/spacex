//
//  SpaceXService.swift
//  SpaceXTests
//
//  Created by Lukas Korinek on 16.09.2021.
//

import XCTest
@testable import SpaceX

class SpaceXServiceTests: XCTestCase {
    var service: SpaceXService!

    override func setUp() {
        super.setUp()
        service = SpaceXService(apiManager: APIManager())
    }

    override func tearDown() {
        service = nil
        super.tearDown()
    }

    func test_Service_fetchLaunches() {
        let exp = expectation(description: "Wait for download data.")
        var launches = [Launch]()
        service.fetchLaunches { result in
            switch result {
            case .success(let data):
                launches = data
                exp.fulfill()
            case .failure(let error):
                print(error)
                XCTFail("Failed to download.")
            }
        }

        waitForExpectations(timeout: 5, handler: nil)
        XCTAssert(launches.count != 0, "Array is empty. Failed to download data.")
    }
}
