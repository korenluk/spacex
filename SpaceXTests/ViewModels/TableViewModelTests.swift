//
//  TableViewModelTests.swift
//  SpaceXTests
//
//  Created by Lukas Korinek on 16.09.2021.
//

import XCTest
@testable import SpaceX

class TableViewModelTests: XCTestCase {

    var viewModel: TableViewModel!

    override func setUp() {
        super.setUp()
        viewModel = TableViewModel(service: SpaceXServiceMock())
    }

    override func tearDown() {
        viewModel = nil
        super.tearDown()
    }

    func test_viewModel_download_launches() {
        viewModel.downloadLaunches()
        XCTAssert(viewModel.numberOfLaunches() != 0, "Count of launches should not be 0")
        XCTAssert(viewModel.launch(at: 1).name == "First", "Name of launch on index 1 should be First")
    }

    func test_viewModel_filter_launches() {
        viewModel.downloadLaunches()
        viewModel.filterLaunches(searchText: "Sec")
        XCTAssert(viewModel.numberOfFilteredLaunches() == 1, "Count of launches should be 1")
        XCTAssert(viewModel.launch(at: 0).name == "Second", "Name of launch on index 0 should be Second")
    }

    func test_viewModel_order_launches() {
        viewModel.downloadLaunches()
        viewModel.orderLanches(by: .nameAsc)
        XCTAssert(viewModel.launch(at: 0).name == "First", "Name of first launch should be First")
        viewModel.orderLanches(by: .nameDesc)
        XCTAssert(viewModel.launch(at: 0).name == "Second", "Name of first launch should be Second")
        viewModel.orderLanches(by: .dateAsc)
        XCTAssert(viewModel.launch(at: 0).date == Date(timeIntervalSince1970: 1631819834),
                  "Date of first launch should be 1631819834")
        viewModel.orderLanches(by: .dateDesc)
        XCTAssert(viewModel.launch(at: 0).date == Date(timeIntervalSince1970: 1600283834),
                  "Date of first launch should be 1600283834")
        viewModel.orderLanches(by: .success)
        XCTAssert(viewModel.launch(at: 0).success, "Success of first launch should be true")
        viewModel.orderLanches(by: .fail)
        XCTAssert(!viewModel.launch(at: 0).success, "Success of first launch should be false")
    }
}
