//
//  SpaceXServiceMock.swift
//  SpaceXTests
//
//  Created by Lukas Korinek on 16.09.2021.
//

import Foundation
@testable import SpaceX

class SpaceXServiceMock: SpaceXServiceType {

    let launches = [Launch(name: "First",
                           links: Links(patch: Patch(small: "img.png"),
                                        webcast: "youtube",
                                        wikipedia: "wiki",
                                        article: "web"),
                           date: Date(timeIntervalSince1970: 1600283834),
                           success: true),
                    Launch(name: "Second",
                           links: Links(patch: Patch(small: "img.png"),
                                        webcast: "youtube",
                                        wikipedia: "wiki",
                                        article: "web"),
                           date: Date(timeIntervalSince1970: 1631819834),
                           success: false)]

    func fetchLaunches(completion: @escaping (Result<[Launch], Error>) -> Void) {
        completion(.success(launches))
    }
}
