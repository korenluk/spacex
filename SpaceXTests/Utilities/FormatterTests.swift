//
//  FormatterTests.swift
//  SpaceXTests
//
//  Created by Lukas Korinek on 16.09.2021.
//

import XCTest
@testable import SpaceX

class FormatterTests: XCTestCase {
    var formatter: SpaceX.Formatter!

    override func setUp() {
        formatter = SpaceX.Formatter()

    }

    override func tearDown() {
        formatter = nil
        super.tearDown()
    }

    func test_formatter_date() {
        let date = Date(timeIntervalSince1970: 1631813025)
        let formatDate = formatter.getYYYYMMDD(from: date)
        XCTAssert(formatDate == "2021.09.16", "Date should be 2021.09.16")
    }

}
