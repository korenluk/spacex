//
//  DetailCoordinator.swift
//  SpaceX
//
//  Created by Lukas Korinek on 15.09.2021.
//

import UIKit

protocol DetailCoordinating: Coordinating {

}

class DetailCoordinator: DetailCoordinating {
    private let resolver: DependencyResolving
    private let launch: Launch
    private weak var navController: UINavigationController?

    init(launch: Launch, navController: UINavigationController, resolver: DependencyResolving) {
        self.launch = launch
        self.resolver = resolver
        self.navController = navController
    }

    deinit {
        print("DETAIL DEINIT")
    }

    func begin() -> UIViewController {
        var viewController = resolver.resolveDetailScene(launch: launch)
        viewController.coordinator = self
        return viewController
    }
}
