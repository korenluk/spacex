//
//  Coordinating.swift
//  SpaceX
//
//  Created by Lukas Korinek on 15.09.2021.
//

import UIKit

protocol Coordinating {
    func begin() -> UIViewController
}
