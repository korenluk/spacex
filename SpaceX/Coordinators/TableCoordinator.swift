//
//  TableCoordinator.swift
//  SpaceX
//
//  Created by Lukas Korinek on 15.09.2021.
//

import UIKit

protocol TableCoordinating {
    func select(launch: Launch)
}

class TableCoordinator: TableCoordinating {
    private let resolver: DependencyResolving
    private weak var navController: UINavigationController?

    init(navController: UINavigationController, resolver: DependencyResolving) {
        self.navController = navController
        self.resolver = resolver
    }

    deinit {
        print("TABLE DEINIT")
    }

    func begin() -> UIViewController {
        var viewController = resolver.resolveTableScene()
        viewController.coordinator = self
        return viewController
    }

    func select(launch: Launch) {
        let coordinator = DetailCoordinator(launch: launch, navController: navController!, resolver: resolver)
        let viewController = coordinator.begin()
        navController?.pushViewController(viewController, animated: true)
    }
}
