//
//  Formatter.swift
//  SpaceX
//
//  Created by Lukas Korinek on 15.09.2021.
//

import Foundation

class Formatter {
    static let shared = Formatter()

    let formatter: DateFormatter

    init() {
        self.formatter = DateFormatter()
    }

    func getYYYYMMDD(from date: Date) -> String {
        self.formatter.dateFormat = "yyyy.MM.dd"
        return formatter.string(from: date)
    }
}
