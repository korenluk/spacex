//
//  TableViewModel.swift
//  SpaceX
//
//  Created by Lukas Korinek on 14.09.2021.
//

import UIKit

class TableViewModel: TableViewModeling {
    weak var delegate: TableViewModelDelegate?

    private var isFetching: Bool = false

    private let service: SpaceXServiceType

    var launches = [Launch]()

    var filteredLaunches = [Launch]()

    init(service: SpaceXServiceType) {
        self.service = service
    }

    func downloadLaunches() {
        fetchLaunches()
    }

    func numberOfLaunches() -> Int {
        launches.count
    }

    func numberOfFilteredLaunches() -> Int {
        filteredLaunches.count
    }

    func launch(at index: Int) -> Launch {
        launches[index]
    }

    func filteredLaunch(at index: Int) -> Launch {
        filteredLaunches[index]
    }

    func filterLaunches(searchText: String) {
        filteredLaunches = launches.filter { launch in
            return launch.name.lowercased().contains(searchText.lowercased())
        }
    }

    func orderLanches(by orderLaunches: OrderLaunches) {
        switch orderLaunches {
        case .nameAsc:
            launches.sort { lhs, rhs in
                lhs.name < rhs.name
            }
        case .nameDesc:
            launches.sort { lhs, rhs in
                lhs.name > rhs.name
            }
        case .dateAsc:
            launches.sort { lhs, rhs in
                lhs.date > rhs.date
            }
        case .dateDesc:
            launches.sort { lhs, rhs in
                lhs.date < rhs.date
            }
        case .success:
            launches.sort { lhs, rhs in
                lhs.success && !rhs.success
            }
        case .fail:
            launches.sort { lhs, rhs in
                !lhs.success && rhs.success
            }
        }
    }
}

private extension TableViewModel {
    func fetchLaunches() {
        guard !isFetching else {
            return
        }

        isFetching = true

        service.fetchLaunches { [weak self] result in
            guard let self = self else {
                return
            }

            self.isFetching = false

            switch result {
            case let .success(launches):
                self.launches = launches
                self.launches.sort { lhs, rhs in
                    lhs.date > rhs.date
                }
                self.delegate?.didDownloadLaunches()
            case let .failure(error):
                self.delegate?.didFail(with: error)
            }
        }
    }
}
