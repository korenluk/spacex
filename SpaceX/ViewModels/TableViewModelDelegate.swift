//
//  TableViewModelDelegate.swift
//  SpaceX
//
//  Created by Lukas Korinek on 14.09.2021.
//

protocol TableViewModelDelegate: AnyObject {
    func didDownloadLaunches()
    func didFail(with error: Error)
}

protocol TableViewModeling: AnyObject {
    var delegate: TableViewModelDelegate? { get set }

    func downloadLaunches()
    func numberOfLaunches() -> Int
    func numberOfFilteredLaunches() -> Int
    func launch(at index: Int) -> Launch
    func filteredLaunch(at index: Int) -> Launch
    func filterLaunches(searchText: String)
    func orderLanches(by orderLaunches: OrderLaunches)
}

public enum OrderLaunches {
    case nameAsc
    case nameDesc
    case dateAsc
    case dateDesc
    case success
    case fail
}
