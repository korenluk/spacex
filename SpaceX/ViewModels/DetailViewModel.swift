//
//  DetailViewModel.swift
//  SpaceX
//
//  Created by Lukas Korinek on 15.09.2021.
//

class DetailViewModel: DetailViewModeling {
    weak var delegate: DetailViewModelDelegate?

    var launch: Launch

    init(launch: Launch) {
        self.launch = launch
    }
}
