//
//  CryptoDetailViewModelDelegate.swift
//  SpaceX
//
//  Created by Lukas Korinek on 15.09.2021.
//

protocol DetailViewModelDelegate: AnyObject {

}

protocol DetailViewModeling: AnyObject {
    var launch: Launch { get }
}
