//
//  DependencyResolving.swift
//  SpaceX
//
//  Created by Lukas Korinek on 15.09.2021.
//

import UIKit

protocol DependencyResolving {
    func resolveAPIManager() -> APIManaging
}

extension DependencyResolving {

    func resolveTableScene() -> TableViewControlling & UIViewController {
        let viewController = TableViewController()
        viewController.viewModel = resolveTableViewModel()
        return viewController
    }

    func resolveTableViewModel() -> TableViewModel {
        return TableViewModel(service: resolveService())
    }

    func resolveDetailScene(launch: Launch) -> DetailViewControlling & UIViewController {
        let viewController = DetailViewController()
        viewController.viewModel = resolveDetailViewModel(launch: launch)
        return viewController
    }

    func resolveDetailViewModel(launch: Launch) -> DetailViewModel {
        return DetailViewModel(launch: launch)
    }

    func resolveService() -> SpaceXService {
        return SpaceXService(apiManager: resolveAPIManager())
    }
}
