//
//  DefaultDependencyContainer.swift
//  SpaceX
//
//  Created by Lukas Korinek on 15.09.2021.
//

class DefaultDependencyContainer: DependencyResolving {
    // MARK: - Managers
    func resolveAPIManager() -> APIManaging {
        return APIManager()
    }
}
