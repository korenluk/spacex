//
//  DetailViewController.swift
//  SpaceX
//
//  Created by Lukas Korinek on 15.09.2021.
//

import UIKit
import SDWebImage

protocol DetailViewControlling {
    var coordinator: DetailCoordinating? { get set}
}

class DetailViewController: UIViewController, DetailViewControlling {
    var coordinator: DetailCoordinating?

    var viewModel: DetailViewModel!

    lazy var name = makeLabel()

    lazy var date = makeLabel()

    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.sd_imageIndicator = SDWebImageActivityIndicator.white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    lazy var articleButton: UIButton = {
        let button = UIButton()
        let config = UIImage.SymbolConfiguration(pointSize: 35)
        button.setImage(UIImage(systemName: "a.square.fill", withConfiguration: config), for: .normal)
        button.tintColor = .white
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(showarticle), for: .touchUpInside)
        return button
    }()

    lazy var wikiButton: UIButton = {
        let button = UIButton()
        let config = UIImage.SymbolConfiguration(pointSize: 35)
        button.setImage(UIImage(systemName: "w.square.fill", withConfiguration: config), for: .normal)
        button.tintColor = .white
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(showWiki), for: .touchUpInside)
        return button
    }()

    lazy var youtubeButton: UIButton = {
        let button = UIButton()
        let config = UIImage.SymbolConfiguration(pointSize: 35)
        button.setImage(UIImage(systemName: "y.square.fill", withConfiguration: config), for: .normal)
        button.tintColor = .white
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(showYoutube), for: .touchUpInside)
        return button
    }()

    lazy var stack: UIStackView = {
        let stack = UIStackView()
        stack.addArrangedSubview(articleButton)
        stack.addArrangedSubview(wikiButton)
        stack.addArrangedSubview(youtubeButton)
        stack.axis = .horizontal
        stack.spacing = 30
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupData()
    }

    func setupUI() {
        view.backgroundColor = .black
        view.addSubview(name)
        view.addSubview(date)
        view.addSubview(imageView)

        view.addSubview(stack)

        navigationItem.title = viewModel.launch.name

        imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50).isActive = true
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 300).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 300).isActive = true

        name.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        name.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 25).isActive = true

        date.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        date.topAnchor.constraint(equalTo: name.bottomAnchor, constant: 25).isActive = true

        stack.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stack.topAnchor.constraint(equalTo: date.bottomAnchor, constant: 25).isActive = true

    }

    func setupData() {
        name.text = "Name: " + viewModel.launch.name
        date.text = "Date: " + Formatter.shared.getYYYYMMDD(from: viewModel.launch.date)
        imageView.sd_setImage(with: URL(string: viewModel.launch.links.patch.small))
    }

    func makeLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        return label
    }

    @objc func showarticle() {
        guard let article = viewModel.launch.links.article else {
            showError()
            return
        }
        UIApplication.shared.open(URL(string: article)!)
    }

    @objc func showWiki() {
        guard let wikipedia = viewModel.launch.links.wikipedia else {
            showError()
            return
        }
        UIApplication.shared.open(URL(string: wikipedia)!)
    }

    @objc func showYoutube() {
        guard let webcast = viewModel.launch.links.webcast else {
            showError()
            return
        }
        UIApplication.shared.open(URL(string: webcast)!)
    }

    func showError() {
        let alertController = UIAlertController(title: "No links",
                                                message: "Sorry, this link is not available for this launch",
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

        present(alertController, animated: true, completion: nil)
    }
}
