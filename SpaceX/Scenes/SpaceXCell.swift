//
//  SpaceXCell.swift
//  SpaceX
//
//  Created by Lukas Korinek on 14.09.2021.
//

import UIKit
import SDWebImage

class SpaceXCell: UITableViewCell {

    var leftImage: UIImageView = {
        var imageView = UIImageView()
        imageView.sd_imageIndicator = SDWebImageActivityIndicator.white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    var rightImage: UIImageView = {
        var imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    var label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        return label
    }()

    var dateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupUI() {
        self.backgroundColor = .black

        addSubview(leftImage)
        addSubview(rightImage)
        addSubview(label)
        addSubview(dateLabel)

        label.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        label.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true

        dateLabel.topAnchor.constraint(equalTo: label.bottomAnchor).isActive = true
        dateLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true

        leftImage.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        leftImage.leftAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leftAnchor).isActive = true
        leftImage.heightAnchor.constraint(equalToConstant: 30).isActive = true
        leftImage.widthAnchor.constraint(equalToConstant: 30).isActive = true

        rightImage.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        rightImage.rightAnchor.constraint(equalTo: self.safeAreaLayoutGuide.rightAnchor).isActive = true
    }

}
