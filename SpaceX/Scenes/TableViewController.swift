//
//  TableViewController.swift
//  SpaceX
//
//  Created by Lukas Korinek on 14.09.2021.
//

import UIKit
import SDWebImage

protocol TableViewControlling {
    var coordinator: TableCoordinating? { get set}
}

class TableViewController: UIViewController, TableViewControlling {
    var coordinator: TableCoordinating?

    var viewModel: TableViewModel!

    lazy var table: UITableView = {
        let table = UITableView()
        table.backgroundColor = .black
        table.separatorColor = .white
        table.separatorInset = .zero
        table.translatesAutoresizingMaskIntoConstraints = false
        table.isHidden = true
        table.register(SpaceXCell.self, forCellReuseIdentifier: "cellid")
        return table
    }()

    lazy var searchController: UISearchController = {
        var searchController = UISearchController()
        searchController.searchBar.placeholder = "Search..."
        searchController.obscuresBackgroundDuringPresentation = false
        return searchController
    }()

    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }

    var isFiltering: Bool {
        return searchController.isActive && !isSearchBarEmpty
    }

    lazy var loadingView: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.color = .white
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.transform = CGAffineTransform(scaleX: 1.75, y: 1.75)
        indicator.startAnimating()
        return indicator
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        table.dataSource = self
        table.delegate = self
        viewModel.delegate = self
        viewModel.downloadLaunches()
    }

    override func viewWillAppear(_ animated: Bool) {
        table.reloadData()
    }

    func setupUI() {
        view.backgroundColor = .black
        navigationItem.title = "SPACE X LAUNCHES"

        view.addSubview(table)
        view.addSubview(loadingView)

        loadingView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loadingView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        table.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        table.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        table.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
        table.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 100

        searchController.searchResultsUpdater = self
        navigationItem.searchController = searchController
        definesPresentationContext = true

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "ORDER",
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(showActionSheet))
    }

    @objc func showActionSheet() {
        let alert = UIAlertController(title: "ORDER BY",
                                      message: "Select by which parameter are launches ordered.",
                                      preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Name Ascending", style: .default, handler: { _ in
            self.viewModel.orderLanches(by: .nameAsc)
            self.table.reloadData()
        }))

        alert.addAction(UIAlertAction(title: "Name Descending", style: .default, handler: { _ in
            self.viewModel.orderLanches(by: .nameDesc)
            self.table.reloadData()
        }))

        alert.addAction(UIAlertAction(title: "Date Ascending", style: .default, handler: { _ in
            self.viewModel.orderLanches(by: .dateAsc)
            self.table.reloadData()
        }))

        alert.addAction(UIAlertAction(title: "Date Descending", style: .default, handler: { _ in
            self.viewModel.orderLanches(by: .dateDesc)
            self.table.reloadData()
        }))

        alert.addAction(UIAlertAction(title: "Success", style: .default, handler: { _ in
            self.viewModel.orderLanches(by: .success)
            self.table.reloadData()
        }))

        alert.addAction(UIAlertAction(title: "Fail", style: .default, handler: { _ in
            self.viewModel.orderLanches(by: .fail)
            self.table.reloadData()
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        }))

        self.present(alert, animated: true, completion: nil)
    }

}

extension TableViewController: TableViewModelDelegate {
    func didDownloadLaunches() {
        table.isHidden = false
        table.reloadData()
        loadingView.stopAnimating()
        loadingView.isHidden = true
    }

    func didFail(with error: Error) {
        let alertController = UIAlertController(title: "ERROR",
                                                message: error.localizedDescription,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Retry", style: .default, handler: { _ in
            self.viewModel.downloadLaunches()
        }))

        present(alertController, animated: true, completion: nil)
    }
}

extension TableViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return viewModel.numberOfFilteredLaunches()
        } else {
            return viewModel.numberOfLaunches()
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // swiftlint:disable:next force_cast
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellid", for: indexPath) as! SpaceXCell

        let launch: Launch
        if isFiltering {
            launch = viewModel.filteredLaunch(at: indexPath.row)
        } else {
            launch = viewModel.launch(at: indexPath.row)
        }

        cell.label.text = launch.name
        cell.dateLabel.text = Formatter.shared.getYYYYMMDD(from: launch.date)
        cell.leftImage.sd_setImage(with: URL(string: launch.links.patch.small))
        cell.rightImage.image = UIImage(systemName: launch.success ? "checkmark.circle.fill" : "xmark.circle.fill")
        cell.rightImage.tintColor = launch.success ? .green : .red
        return cell
    }
}

extension TableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let launch: Launch
        if isFiltering {
            launch = viewModel.filteredLaunch(at: indexPath.row)
        } else {
            launch = viewModel.launch(at: indexPath.row)
        }
        coordinator?.select(launch: launch)
    }
}

extension TableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        viewModel.filterLaunches(searchText: searchController.searchBar.text!)
        table.reloadData()
    }
}
