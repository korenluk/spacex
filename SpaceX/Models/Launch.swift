//
//  Launches.swift
//  SpaceX
//
//  Created by Lukas Korinek on 14.09.2021.
//

import Foundation

struct Launch: Codable, Comparable {
    let name: String
    let links: Links
    let date: Date
    let success: Bool

    private enum CodingKeys: String, CodingKey {
        case name = "name"
        case links = "links"
        case date = "date_unix"
        case success = "success"
    }

    static func < (lhs: Launch, rhs: Launch) -> Bool {
        lhs.date < rhs.date
    }

    static func == (lhs: Launch, rhs: Launch) -> Bool {
        lhs.name == rhs.name
    }
}

struct Links: Codable {
    let patch: Patch
    let webcast: String?
    let wikipedia: String?
    let article: String?
}

struct Patch: Codable {
    let small: String
}
