//
//  SpaceXServiceType.swift
//  SpaceX
//
//  Created by Lukas Korinek on 16.09.2021.
//

import Foundation

protocol SpaceXServiceType {
    func fetchLaunches(completion: @escaping (Result<[Launch], Error>) -> Void)
}
