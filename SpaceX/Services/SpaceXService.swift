//
//  SpaceXService.swift
//  SpaceX
//
//  Created by Lukas Korinek on 14.09.2021.
//

import Foundation

class SpaceXService: SpaceXServiceType {
    let apiManager: APIManaging

    init(apiManager: APIManaging) {
        self.apiManager = apiManager
    }

    func fetchLaunches(completion: @escaping (Result<[Launch], Error>) -> Void) {
        apiManager.request(request: SpaceXRouter.launches, completion: completion)
    }
}
