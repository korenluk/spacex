//
//  APIManager.swift
//  SpaceX
//
//  Created by Lukas Korinek on 09.09.2021.
//

import Foundation
import Alamofire

final class APIManager {
    let sessionManager: Session

    lazy var jsonDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        return decoder
    }()

    static var defaultSessionManager: Session {
        return Session.default
    }

    init(sessionManager: Session = APIManager.defaultSessionManager) {
        self.sessionManager = sessionManager
    }
}

extension APIManager: APIManaging {
    func request<T: Decodable>(request: Alamofire.URLRequestConvertible,
                               completion: @escaping (Swift.Result<T, Error>) -> Void) {
        sessionManager.request(request).validate().responseData(completionHandler: { [weak self] response in
            guard let `self` = self else {
                return
            }

            switch response.result {
            case let .success(response):
                do {
                    let object = try self.jsonDecoder.decode(T.self, from: response)
                    completion(.success(object))
                } catch {
                    // parsing error
                    completion(.failure(error))
                }
            case let .failure(requestError):
                // try parse server error if any data, else return request error
                guard let data = response.data else {
                    completion(.failure(requestError))
                    return
                }
                do {
                    let apiError = try self.jsonDecoder.decode(APIError.self, from: data)
                    completion(.failure(apiError))
                } catch {
                    completion(.failure(requestError))
                }
            }
        })
    }
}
