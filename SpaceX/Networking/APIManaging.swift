//
//  APIManaging.swift
//  SpaceX
//
//  Created by Lukas Korinek on 09.09.2021.
//

import Foundation
import Alamofire

protocol APIManaging {
    func request<T: Decodable>(request: Alamofire.URLRequestConvertible,
                               completion _: @escaping (Swift.Result<T, Error>) -> Void)
}
