//
//  SpaceXRouter.swift
//  SpaceX
//
//  Created by Lukas Korinek on 09.09.2021.
//

import Foundation
import Alamofire

enum SpaceXRouter: URLRequestConvertible {
    case launches

    var path: String {
        switch self {
        case .launches:
            return "/launches/past"
        }
    }

    var parameters: [String: Any] {
        switch self {
        case  .launches:
            return [:]
        }
    }

    var method: HTTPMethod {
        switch self {
        case .launches:
            return .get
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = URL(fileURLWithPath: "https://api.spacexdata.com/v5")

        var request = URLRequest(url: url.appendingPathComponent(path))

        request.httpMethod = method.rawValue

        let finalRequest = try URLEncoding.default.encode(request, with: parameters)
        return finalRequest
    }
}
